package sim.tsa;

import java.util.ArrayList;

import uchicago.src.sim.space.Object2DTorus;
import cern.jet.random.Uniform;
import chrisxue.tsplib.TSPLib;

public class TSAWorldGenerator {
  
  public static void random(int num, int width, int height, ArrayList<TSACity> list, Object2DTorus cities) {
    for (int i = 0; i < num; i++) {
      int x, y;
      do {
        x = Uniform.staticNextIntFromTo(0, width - 1);
        y = Uniform.staticNextIntFromTo(0, height - 1);
      } while (cities.getObjectAt(x, y) != null); // don't place two cities onto the same place
      
      TSACity city = new TSACity(i, x, y); // contruct new city (in a day, unlike Rome)
      cities.putObjectAt(x, y, city); // store it in the grid
      list.add(city); // ... and in the list
    }
  }
  
  public static void tsplib(String path, int width, int height, ArrayList<TSACity> list, Object2DTorus cities) {
    TSPLib tsplib = new TSPLib(path);
    
    double left = tsplib.data[0].x;
    double right = tsplib.data[0].x;
    double top = tsplib.data[0].y;
    double bottom = tsplib.data[0].y;
    
    for (int i = 1; i < tsplib.dimension; i++) {
      double x = tsplib.data[i].x;
      double y = tsplib.data[i].y;
      
      if (x < left) left = x;
      else if (right < x) right = x;
      if (y < top) top = y;
      else if (bottom < y) bottom = y;
    }
    
    double mapWidth = right - left;
    double mapHeight = bottom - top;
    double mapRatio = mapWidth / mapHeight;
    double ratio = (double)width / height;
    double scale = mapRatio < ratio ? height / mapHeight : width / mapWidth;
    
    for (int i = 0; i < tsplib.dimension; i++) {
      double x = tsplib.data[i].x;
      double y = tsplib.data[i].y;
      
      int screenX = (int) Math.round(x * scale);
      int screenY = (int) Math.round(y * scale);
      
      TSACity city = new TSACity(i, x, y, scale);
      cities.putObjectAt(screenX, screenY, city);
      list.add(city);
    }
  }

}
