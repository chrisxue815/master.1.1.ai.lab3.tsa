package sim.tsa;

import uchicago.src.sim.gui.*;

public class TSACity implements Drawable {

  private final int num; // identifier (0-n for indexing into arrays)
  private final int x; // coordinates in screen space
  private final int y;
  private final double realX; // coordinates in map space
  private final double realY; // coordinates in map space
  
  public TSACity(int num, double realX, double realY) {
    this(num, realX, realY, 1);
  }

  public TSACity(int num, double realX, double realY, double scale) {
    this.num = num;
    this.realX = realX;
    this.realY = realY;
    this.x = (int) Math.round(realX * scale);
    this.y = (int) Math.round(realY * scale);
  }

  public int getNum() { return num; }
  public int getX() { return x; }
  public int getY() { return y; }
  public double getRealX() { return realX; }
  public double getRealY() { return realY; }
  
  @Override
  public void draw(SimGraphics g) { g.drawFastRoundRect(java.awt.Color.green); }
}
