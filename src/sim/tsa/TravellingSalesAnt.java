package sim.tsa;

import java.util.ArrayList;

import cern.jet.random.Uniform;

/* TravellingSalesAnt. Practical 3: Implement an agent capable of
 * finding a path through the space (step()) and updating the
 * pheromone levels proportionally to the quality of the solution
 * (step2()), according to an ant colony optimisation strategy.
 *
 */

public class TravellingSalesAnt {

  private TSAWorld space;
  private TSAModel model;
  private double length;
  private ArrayList<TSACity> visited;

  public TravellingSalesAnt(TSAWorld space, TSAModel model) {
    this.space = space;
    this.model = model;
  }

  public ArrayList<TSACity> getPath() { return visited; }
  public double getLength() { return length; }

  public void step(TSACity start) {
    // add the starting city to visited, other cities to unvisited
    visited = new ArrayList<TSACity>();
    visited.add(start);
    ArrayList<TSACity> unvisited = (ArrayList<TSACity>) space.getCitiesList().clone();
    unvisited.remove(start);
    
    // loop until all cities are visited
    while (unvisited.size() > 0) {
      int numUnvisited = unvisited.size();
      double[] decisionTable = new double[numUnvisited];
      double totalPossibility = 0;
      
      for (int i = 0; i < numUnvisited; i++) {
        TSACity other = unvisited.get(i);
        double pheromone = space.getPheromone(start, other);
        double distance = space.getDistance(start, other);
        
        double pheromoneWeighted = Math.pow(pheromone, model.getPheromoneWeight());
        double distanceWeighted = Math.pow(distance, model.getDistanceWeight());
        
        // distribute possibility to each city
        totalPossibility += pheromoneWeighted / distanceWeighted;
        decisionTable[i] = totalPossibility;
      }

      // throw a ball to all cities
      double random = Uniform.staticNextDoubleFromTo(0, totalPossibility);
      
      for (int i = 0; i < numUnvisited; i++) {
        // if the ball drops in this city
        if (random <= decisionTable[i]) {
          start = unvisited.get(i);
          visited.add(start);
          unvisited.remove(i);
          break;
        }
      }
    }
    
    // calculate the length of the chosen path
    length = 0;
    int numCities = visited.size();
    for (int i = 1; i < numCities; i++) {
      length += space.getDistance(visited.get(i - 1), visited.get(i));
    }
  }

  public void step2() {
    int numCities = visited.size() - 1;
    
    // deposit pheromone to visited paths
    for (int i = 1; i < numCities; i++) {
      space.deposit(visited.get(i - 1), visited.get(i), model.getPheromoneAmount());
    }
  }
}
