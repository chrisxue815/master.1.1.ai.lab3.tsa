package chrisxue.tsplib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TSPLib {
  
  public enum Type {
    TSP, ATSP, SOP, HCP, CVRP, TOUR
  }
  
  public enum EdgeWeightType {
    EXPLICIT, EUC_2D, EUC_3D, MAX_2D, MAX_3D,
    MAN_2D, MAN_3D, CEIL_2D, GEO, ATT,
    XRAY1, XRAY2, SPECIAL
  }
  
  public enum EdgeWeightFormat {
    FUNCTION, FULL_MATRIX, UPPER_ROW, LOWER_ROW, UPPER_DIAG_ROW,
    LOWER_DIAG_ROW, UPPER_COL, LOWER_COL, UPPER_DIAG_COL, LOWER_DIAG_COL
  }
  
  public enum EdgeDataFormat {
    EDGE_LIST, ADJ_LIST
  }
  
  public enum NodeCoordType {
    TWOD_COORDS, THREED_COORDS, NO_COORDS
  }
  
  public enum DisplayDataType {
    COORD_DISPLAY, TWOD_DISPLAY, NO_DISPLAY
  }
  
  public class Coordinate {
    public double x;
    public double y;
  }
  
  public String name;
  public String type;
  public String comment;
  public int dimension;
  public int capacity;
  public String edgeWeightType;
  public String edgeWeightFormat;
  public String edgeDataFormat;
  public String nodeCoordType;
  public String displayDataType;
  public Coordinate[] data;
  
  public TSPLib(String path) {
    File file = new File(path);
    BufferedReader reader = null;
    
    try {
      reader = new BufferedReader(new FileReader(file));
      String tmp;
      
      while ((tmp = reader.readLine()) != null) {
        tmp = tmp.trim();
        
        if ("NODE_COORD_SECTION".equals(tmp)) break;
        
        String[] str = tmp.split(":");
        
        if (str.length != 2) continue;
        
        String key = str[0].trim();
        String value = str[1].trim();
        
        if ("NAME".equals(key)) {
          name = value;
        }
        else if ("TYPE".equals(key)) {
          type = value;
        }
        else if ("COMMENT".equals(key)) {
          comment = value;
        }
        else if ("DIMENSION".equals(key)) {
          dimension = Integer.parseInt(value);
        }
        else if ("CAPACITY".equals(key)) {
          capacity = Integer.parseInt(value);
        }
        else if ("EDGE_WEIGHT_TYPE".equals(key)) {
          edgeWeightType = value;
        }
        else if ("EDGE_WEIGHT_FORMAT".equals(key)) {
          edgeWeightFormat = value;
        }
        else if ("EDGE_DATA_FORMAT".equals(key)) {
          edgeDataFormat = value;
        }
        else if ("NODE_COORD_TYPE".equals(key)) {
          nodeCoordType = value;
        }
        else if ("DISPLAY_DATA_TYPE".equals(key)) {
          displayDataType = value;
        }
      }
      
      if (!"TSP".equals(type) || !"EUC_2D".equals(edgeWeightType)) {
        throw new Exception("Unsupported type \"" + type + "\", expected \"TSP\". Or unsupported edge weight type \"" + edgeWeightType + "\", expected \"EUC_2D\".");
      }
      
      data = new Coordinate[dimension];
      
      while ((tmp = reader.readLine()) != null) {
        final String floatPattern = "([-+]?[0-9]*\\.?[0-9]+(?:[eE][-+]?[0-9]+)?)";
        final String pattern = "(\\d+) +" + floatPattern + " +" + floatPattern;
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(tmp);

        if (!m.find() || m.groupCount() != 3) continue;
        
        int i = Integer.parseInt(m.group(1)) - 1;
        data[i] = new Coordinate();
        data[i].x = Double.parseDouble(m.group(2));
        data[i].y = Double.parseDouble(m.group(3));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      try {
        reader.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
